import { reverseDate } from "../../../../utils/Utils";

const DateDeadlineSpan = ({date}) => {
    return (
            <span data-toogle="time">{reverseDate(date)}</span>
    );
}

export default DateDeadlineSpan;